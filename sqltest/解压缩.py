
#使用rarfile库破解rar格式压缩包密码
import rarfile

rar_file_name = 'ZAQ.rar'
pwd_file_name = 'pwd.txt'

# 将字典中的所有密码读入内存
with open(pwd_file_name, 'r') as f:
    passwords = f.readlines()

# 去除每个密码后面的换行符
passwords = [pwd.strip() for pwd in passwords]

# 逐个尝试密码，直到找到正确的密码或者尝试完所有密码
for password in passwords:
    try:
        with rarfile.RarFile(rar_file_name) as rar_file:
            # 设置密码后解压缩文件
            rar_file.extractall(pwd=password.encode('utf-8'))
        print('密码为：', password)
        break
    except Exception as e:
        continue