import paramiko
# 执行多条命令,注意传入的参数有个list
def execMultiCmd(host, user, psw, cmds: list, port=22) -> (str, str):
    with paramiko.SSHClient() as ssh_client:
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=host, port=port, username=user, password=psw)
        cmd = ";".join(cmds)
        _, stdout, stderr = ssh_client.exec_command(cmd, get_pty=True)
        result = stdout.read().decode('utf-8')
        err = stderr.read().decode('utf-8')
    return result, err


if __name__ == '__main__':
    cmdList = ['cd /var/opt/jhga', 'ls', 'sudo systemctl restart comparedQuery comparedQueryExcutor algoServer apiServer irislib']
    print(execMultiCmd("192.168.91.20", "root", "jHrd3344", cmdList))
    print(execMultiCmd("192.168.91.21", "root", "jHrd3344", cmdList))
    print(execMultiCmd("192.168.91.24", "root", "jHrd3344", cmdList))
    print(execMultiCmd("192.168.91.25", "root", "jHrd3344", cmdList))
    print(execMultiCmd("192.168.91.26", "root", "jHrd3344", cmdList))
    cmdList1 = ['cd /var/opt/fakePlcIris2', 'sudo systemctl restart fakePlcIris.service']
    print(execMultiCmd("192.168.91.44", "root", "jHrd3344", cmdList1))